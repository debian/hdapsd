Source: hdapsd
Section: misc
Priority: optional
Maintainer: Evgeni Golov <evgeni@debian.org>
Build-Depends: debhelper-compat (= 13), libconfig-dev, pkgconf, systemd-dev
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian/hdapsd
Vcs-Git: https://salsa.debian.org/debian/hdapsd.git
Homepage: https://github.com/evgeni/hdapsd
Rules-Requires-Root: no

Package: hdapsd
Architecture: i386 amd64 powerpc
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Recommends: tp-smapi-dkms [!powerpc]
Description: HDAPS daemon for various laptops with motion sensors
 This is a disk protection user-space daemon. It monitors the acceleration
 values through an interface and automatically initiates disk head parking
 if a fall or sliding of the laptop is detected.
 .
 Currently, the following interfaces are supported:
  * IBM/Lenovo ThinkPad (HDAPS)
  * Apple iBook/PowerBook (AMS)
  * Apple MacBook/MacBook Pro (APPLESMC)
  * HP (HP3D)
  * Dell (FREEFALL)
  * Toshiba (ACPI and HAPS)
  * Acer (INPUT)
 .
 On ThinkPads, it is recommended that you use this daemon with the hdaps
 module provided by tp-smapi rather the one in the kernel, as this will save
 you a bit of power and will work on a wider range of ThinkPads.
